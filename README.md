# Convolutional neural networks.Recognition of formation energy in alloys
With the assistance of a convolutional neural network (CNN), we predict the formation energies of crystal structures belonging to the half-Heusler alloys with the formula (Nb_xTa_{1-x})_{1-z}Vac_zCoSb and Nb_xV_{1-x})_{1-z}Vac_zCoSb, where Vac is the % of vacancy defects. We first create atomic distribution using a combination of quantum mechanics and statistical physics [Miranda, Phys. Rev. B 101, 064201 (2020), Miranda and Gruhn, J. Mater. Chem. A, 2021,9, 21111]. The labeled energy images of these atomic distributions at different compositions and temperatures are fed into the CNN.

We also model throughout the training in parallel with a CNN and NN, the addition of extra information to the images, and the values of temperature and/or atomic composition. In a final model, we use input images and temperature and try to predict the energy and composition.

# Introduction

In our previous studies of Half-heusler alloy, we applied a multi-scale method to study atomic distributions in (Nb$_x$V$_{1-x}$)$_{1-z}$Vac$_z$CoSb where we linked quantum mechanical (density function theory, DFT) and statistical (Monte Carlo sampling) calculations by an atomic cluster expansion. Here we showed that there is a complex arrangement of Nb and V atoms that could be key to manipulating thermal conductivity without strongly deteriorating the electric transport, both types of conductions are crucial to obtaining a high figure of merit (ZT). 

In this work, we generate a total of 4321 images of atomic distributions by the Monte Carlo method. For all images, we have information about their formation energy at given alloy compositions and temperatures. We use the energy as labeled data to train a CNN to predict these energies of formation or the energy and temperature to predict the energy of formation and composition.

# Programs

We address the problem by first creating three CNN models for each individual set of data with formula  Nb$_{0.40}$Ta$_{0.40}$Vac$_{0.8}$CoSb and Nb$_{0.16}$Ta$_{0.64}$Vac$_{0.8}$CoSb concentrations. Then, we create a single CNN model to identify all three concentrations in a single model. This is presented in the slide convol_NN_NbTaVavCoSb.pdf.

For the general case of identifying simultaneously the formation energies of the two types of alloys (Nb$_x$Ta$_{1-x}$)$_{1-z}$Vac$_z$CoSb and (Nb$_x$V$_{1-x}$)$_{1-z}$Vac$_z$CoSb, which is in the Jupyten book reviewed.

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  1 09:08:47 2022

@author: joaquin
"""

import os
# import re
# import glob
import imageio as iio
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps

from tensorflow import keras
import tensorflow as tf
from sklearn.model_selection import train_test_split

# from tensorflow.keras.preprocessing.image import ImageDataGenerator
# from tensorflow.keras.preprocessing.image import img_to_array

################## Preprocessing Images ################3
####### Remove white edges and Jmol logo  ###########
file_src0="snapshots_trimmed"
for x in os.listdir(file_src0):
    img = Image.open(os.path.join(file_src0, x))
    cropped = img.crop((9.5, 9.5, 264.75, 264.75))  
    cropped.save(os.path.join(file_src0, x))

######### rotate 90 grades  $##############
# The 4 views 1,3,4,5,6 have same orientation  as views 0,2  ##########

file_src0="snapshots_trimmed"
file_src1="snapshots1345_90rot"
for x in os.listdir(file_src0):
    img = Image.open(os.path.join(file_src0, x))
    rotate_img = img.rotate(90)  
    rotate_img.save(os.path.join(file_src1, x))

######### rotate 180 grades  $##############
# Double the amount of data by rotating 180 every snapshot
file_src2="snapshots"
file_src3="snapshots_rotate180"
for x in os.listdir(file_src2):
    img = Image.open(os.path.join(file_src2, x))
    rotate_img = img.rotate(180)  
    rotate_img.save(os.path.join(file_src3, x))


snaps = []          # list with images
temperatures = []   # list temperatures
y = []              # list energies eV

# nbs = []            # list porcentage of Nb atoms
# vacs = []           # list porcentage of vacancies
# views = []          # list  direction of snapshots
# views.append(vi)
# nbs.append(n)
# vacs.append(va)    

# Read all the image files from folder snapshots. The files names have
# the values of temperature, composition and energy. Split values, get them
#  and fill lists with  images and values.


file_src="snapshots/"
for x in os.listdir(file_src):
#        print(os.path.basename(x))
        energy =' _'.join(x.split('_')[4:5])
        temper = ' _'.join(x.split('_')[0:1])
        temper = (temper - 300)/5900  # Temperature normalization
        y.append(float(energy))
        temperatures.append(float(temper))
        snap = iio.imread(file_src + x)
        snaps.append(snap)

for i in range(4):
    plt.subplot(2, 2, i+1)
    plt.imshow(snaps[i])
    plt.axis('off')
    pair = (temperatures[i], y[i] )
    plt.title(pair, fontdict={'size': 8})
plt.show()

# X1 = img_to_array(snaps[0])
# X = X1.reshape(1, 510, 510, 3)

X = np.array(snaps)
y = np.asarray(y)
# X_train = np.array(snaps_train)
# y_train = np.asarray(y_train)
#X = snaps_array.reshape(120, 510, 510, 3)

X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=0.50,
                                                    random_state=42)

# # Einen Batch aus den Trainingsdaten abrufen
# batch = train_data.take(1)
# print('batch:', batch)
# print()

model = keras.models.Sequential()

########### Modeling using predeterminated Netzwerk   ##############

# # # MobileNet V2
# mobilenet_v2 = "https://tfhub.dev/google/tf2-preview/mobilenet_v2/feature_vector/4"
# base_model = hub.KerasLayer(mobilenet_v2,
#     input_shape=(255,255,3))

# MobileNet V1
base_model = keras.applications.MobileNet(
    input_shape=(255, 255, 3),
    alpha=1,              # Anzahl der Kanäle um einen Faktor verringern
    depth_multiplier=1,     # Anzahl der Schichten um einen Faktor vergrößern
    dropout=0.001,          # Dropout vor der Ausgabeschicht
    include_top=False,       # Wir wollen die Ausgabeschicht entfernen
    weights="imagenet",      # Entweder von 'imagenet' oder None
    input_tensor=None,       # Kann als Eingabe in das Modell verwendet werden
    pooling='avg',            # Wenn include_top=False kann eine Pooling-Schicht hinzugefügt werden
    # Wenn include_top=True, und weights=None, dann kann die Anzahl der Ausgabeneuronen eingestellt werden
    classes=1000,
    classifier_activation="softmax")  # Nur wenn include_top=True

base_model.summary()

###########  Output layers   ###########
model.add(base_model)
model.add(keras.layers.Dense(256, activation='relu'))
model.add(keras.layers.Dense(128, activation='relu'))
model.add(keras.layers.Dense(64, activation='relu'))
model.add(keras.layers.Dense(32, activation='relu'))
model.add(keras.layers.Dense(1, activation='linear'))  

model.summary()

def r2_score(y_true, y_pred):
    y_diff = tf.reduce_sum(tf.square(y_true-y_pred))
    y_square = tf.reduce_sum(tf.square(y_true-tf.reduce_mean(y_true)))
    return 1-y_diff/y_square


model.compile(loss='mse', optimizer='adam', metrics=r2_score)

base_model.trainable = False
model.summary()

history = model.fit(X_train, y_train, epochs=100, batch_size=12,
                    validation_data=(X_test, y_test))

model.save('p255_MobiNetD4_batch12_5200-300_m5050.h5')

# Lernkurven darstellen
plt.plot(history.history['r2_score'], label='Train data')
plt.plot(history.history['val_r2_score'], label='Test data')
plt.xlabel('Epoch')
plt.ylabel('R2')
plt.xlim(0,100)
plt.ylim(-0.5,1)
plt.legend()
plt.title("p255_MobiNetD4_batch12_5200-300_m5050")
plt.tight_layout()
plt.savefig('p255_MobiNetD4_batch12_5200-300_m5050.png')
plt.show()

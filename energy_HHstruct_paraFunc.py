#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 13:21:31 2022

@author: joaquin

"""
import os
# import re
# import glob
import imageio as iio
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from tensorflow import keras
import tensorflow as tf
from sklearn.model_selection import train_test_split
from keras.models import Model
from PIL import Image, ImageOps
#from keras.utils import plot_model

snaps = []          # list with images [(255 pixels,255 pixels)]
temperatures = []   # list temperatures [K]
nbs = []            # list porcentage of Nb atoms [0.0 - 1.0]
tas = []            # list porcentage of Ta atoms [0.0 - 1.0]
y = []              # list energies eV
        
file_src="snapshots/"
for x in os.listdir(file_src):
#        print(os.path.basename(x))
        energy =' _'.join(x.split('_')[4:5])
        temper = ' _'.join(x.split('_')[0:1])
        nb = ' _'.join(x.split('_')[2:3])
        nb = float(nb)
        ta = 0.8 - nb
        temper = float(temper)
        temper = (temper - 300)/5900  # Temperature normalization
        nbs.append(nb)
        tas.append(ta)
        y.append(float(energy))
        temperatures.append(temper)
        snap = iio.imread(file_src + x)
        snaps.append(snap)
       
# Plot 4 images
for i in range(4):
    plt.subplot(2, 2, i+1)
    plt.imshow(snaps[i])
    plt.axis('off')
    pair = (temperatures[i], y[i] )
    plt.title(pair, fontdict={'size': 8})
plt.show()

# Feature Images
Xim = np.array(snaps)

# 3 Features 
temperatures =np.array(temperatures)
nbs = np.array(nbs)
tas = np.array(tas)

#list_data = [temperatures, nbs, tas]
#X = pd.DataFrame (temperatures, columns = ['Temperatures'])
# X['Nb_concent'] = df_nb['Nb_cocent']

data = {"Temperatures":temperatures, "Nb_concent":nbs, "Ta_concent":tas}
X = pd.DataFrame(data)
X =  X.values

X = X.astype('float')
y = np.array(y).astype('float')

del nbs, tas, temperatures, data, snaps, ta, nb, temper, snap, energy


Xim_train, Xim_test, y_train, y_test = train_test_split(Xim, y, train_size=0.50,
                                                    random_state=42)

X_train, X_test, __, __ = train_test_split(X, y, train_size=0.50,
                                                    random_state=42)

# 1st Model
# Input for MobileNet
visible = keras.layers.Input(shape=(255, 255, 3))

# MobileNet V1
convol = keras.applications.MobileNet( #input_shape=(255, 255, 3),
    alpha=1,              
    depth_multiplier=1,     
    dropout=0.001,          
    include_top=False,       
    weights="imagenet",      
    input_tensor=None,       
    pooling='avg',           
    classes=1000,
    classifier_activation="softmax")(visible)

output_conv = keras.layers.Dense(128, activation='linear',
                                 name="Output_Conv")(convol)

# 2nd Model
# Input for hidden model
temp_conc = keras.layers.Input(shape=(3,))

hidden1 = keras.layers.Dense(3, name="hidden1")(temp_conc)
hidden = keras.layers.Dense(60, name="hidden2")(hidden1)

output_hidden = keras.layers.Dense(32, name="Output_Hidden")(hidden) 

# merge input models
merge = tf.keras.layers.concatenate([output_conv, output_hidden])

# interpretation model
inter1 = keras.layers.Dense(64, activation='relu', name="inter1")(merge)
inter2 = keras.layers.Dense(32, activation='relu', name="inter2")(inter1)

output = keras.layers.Dense(1, activation='linear', name="Output")(inter2)

model = Model(inputs=[visible, temp_conc], outputs=output) 

model.summary() 

#plot_model(model, to_file='multiple_inputs.png')


def r2_score(y_true, y_pred):
    y_diff = tf.reduce_sum(tf.square(y_true-y_pred))
    y_square = tf.reduce_sum(tf.square(y_true-tf.reduce_mean(y_true)))
    return 1-y_diff/y_square

model.compile(loss='mse', optimizer='adam', metrics=r2_score)

history = model.fit( [Xim_train, X_train.astype('float')], y_train, 
                    batch_size= 12,
                    epochs = 50,
                    validation_data = ([Xim_test, X_test.astype('float')], y_test) )


model.save('p255_MobiNetD4_batch12_epoch50_6200-300_m5050_Par.h5')

# Lernkurven darstellen
plt.plot(history.history['r2_score'], label='Train data')
plt.plot(history.history['val_r2_score'], label='Test data')
plt.xlabel('Epoch')
plt.ylabel('R2')
plt.xlim(0,50)
plt.ylim(-0.5,1)
plt.legend()
plt.title("p255_MobiNetD4_batch12_epoch50_m5050_Par")
plt.tight_layout()
plt.savefig('p255_MobiNetD4_batch12_epoch50_m5050_Par.png')
plt.show()
